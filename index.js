'use strict';
//document.domain = "bitbucket.org";


var orgInput = document.getElementById('org'),
    orgForm = document.getElementById('org-form');

orgForm.onsubmit = loadFromInput;

function loadFromInput() {
    loadOrganization(orgInput.value);
    return false;
}

function loadOrganization(org) {
    //https://bitbucket.org/api/2.0/repositories/django
    getRepos('https://bitbucket.org/api/2.0/repositories/' + org );
}

function getRepos(url) {
    var xhr = new XMLHttpRequest();
    xhr.onload = onResponse;
    xhr.open('get', url, true);
    xhr.send();
    xhr.responseType = 'json';
}

function onResponse(e) {

    var xhr = e.target;
    if (xhr.response.message === 'Not Found') {
        console.log(orgInput.value + ' user not found');
        return;
    }

    addRepos(xhr.response);

    var links = getLinks(xhr.getResponseHeader('Link'));
    if (links && links.next) getRepos(links.next);

    document.body.className = 'loaded';
}

function getLinks(header) {
    if (!header) return null;

    var parts = header.split(','),
        links = {};

    for (var i = 0; i < parts.length; i++) {
        var section = parts[i].split(';'),
            url = section[0].match(/<(.*)>/)[1],
            name = section[1].match(/rel="(.*)"/)[1];
        links[name] = url;
    }
    return links;
}

function addRepos(repos) {

    for (var i = 0; i < repos.length; i++) {
        var repo = repos[i];
        console.log(repo.name);
    }

    
}

